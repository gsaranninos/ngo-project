from ngoapp.ngo.models import checkoutbanner,contactbanner,gallerybanner,projectbanner,volunteerbanner,aboutbanner,aboutus,hometestimonial,banner,volunteers,orders,checkout,productcolorimage,UserProfileInfo,usertokens,enquiry,gallerycategory,gettouchwithus,Usercategory,projects,categories,tags,blog_tags,blogs,productadd,colors
from django.contrib.auth.models import User
from django.core.files.images import get_image_dimensions
from django import forms
from django.forms.widgets import TextInput

class UForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','password','email')

class UserProfileInfoForm(forms.ModelForm):
 
    class Meta():
        model = UserProfileInfo
        fields = ["first_name", "last_name"]

class usertokensForm(forms.ModelForm):
    class Meta:  
        model = usertokens 
        fields = "__all__"

class enquiryForm(forms.ModelForm):  
    class Meta:  
        model = enquiry
        fields= ["name", "email","phone","subject","msg"] 


class galleryCategoryForm(forms.ModelForm):
    class Meta:  
        model = gallerycategory 
        fields = ["image_file","category"]


    def clean_image_file(self):
        image_file = self.cleaned_data.get('image_file')
        if image_file:
            w, h = get_image_dimensions(image_file)
            if w < 500:
                raise forms.ValidationError("The image is %i pixel wide. It's supposed to be 1000px" % w)
            if h < 500:
                raise forms.ValidationError("The image is %i pixel high. It's supposed to be 1000px" % h)
        return image_file

class UserCategoryForm(forms.ModelForm):
    class Meta:  
        model = Usercategory 
        fields = ["name"]
        
    def clean_name(self):
        name = self.cleaned_data.get('name')
        name_qs = Usercategory.objects.filter(name=name)
        if name_qs.exists():
            raise forms.ValidationError("Name already exists")
        return name


class projectsForm(forms.ModelForm):
    class Meta:  
        model = projects 
        fields =  "__all__"


class volunteersForm(forms.ModelForm):
    class Meta:  
        model = volunteers 
        fields =  "__all__"        

class checkoutForm(forms.ModelForm):
    class Meta:  
        model = checkout   
        fields='__all__'

    def clean_quantity(self):
        quantity = self.cleaned_data.get('quantity')
        if quantity==0:
            raise forms.ValidationError("Choose files")
        return quantity 
        
    
class ordersForm(forms.ModelForm):
    class Meta:  
        model = orders   
        fields='__all__'


class CategoriesForm(forms.ModelForm):
    class Meta:  
        model = categories 
        fields = ["Name","meta_title","meta_keyword"]
        
    def clean_Name(self):
        Name = self.cleaned_data.get('Name')
        Name_qs = categories.objects.filter(Name=Name)
        if Name_qs.exists():
            raise forms.ValidationError("Name already exists")
        return Name



class tagsForm(forms.ModelForm):
    class Meta:  
        model = tags 
        fields = ["Name","meta_title","meta_keyword"]


    def clean_Name(self):
        Name = self.cleaned_data.get('Name')
        Name_qs = tags.objects.filter(Name=Name)
        if Name_qs.exists():
            raise forms.ValidationError("Name already exists")
        return Name



class blog_tagsForm(forms.ModelForm):
    class Meta:  
        model = blog_tags 
        fields = ["blog","tag"]


class blogsForm(forms.ModelForm):
    class Meta:  
        model = blogs   
        fields=["category","title","description","image_file","image_alt","image_title","meta_title","meta_keyword"]        



class gettouchwithusForm(forms.ModelForm):
    class Meta:  
        model = gettouchwithus   
        fields='__all__'


class colorsForm(forms.ModelForm):
    class Meta:
        model = colors
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'}),
        }

class productaddForm(forms.ModelForm):
    class Meta:  
        model = productadd   
        fields='__all__'        

class productcolorimageForm(forms.ModelForm):
    class Meta:  
        model = productcolorimage   
        fields='__all__'
        
class bannerForm(forms.ModelForm):
    class Meta:  
        model = banner 
        fields = ["image_file","title"]        
        
class hometestimonialForm(forms.ModelForm):
    class Meta:  
        model = hometestimonial 
        fields = ["image_file","contenttext"]  

class aboutusForm(forms.ModelForm):
    class Meta:  
        model = aboutus 
        fields =  "__all__"    
        

class aboutbannerForm(forms.ModelForm):
    class Meta:  
        model = aboutbanner 
        fields = ["image_file","title"]        

class projectbannerForm(forms.ModelForm):
    class Meta:  
        model = projectbanner 
        fields = ["image_file","title"]

class volunteerbannerForm(forms.ModelForm):
    class Meta:  
        model = volunteerbanner 
        fields = ["image_file","title"]

class gallerybannerForm(forms.ModelForm):
    class Meta:  
        model = gallerybanner 
        fields = ["image_file","title"]           

class contactbannerForm(forms.ModelForm):
    class Meta:  
        model = contactbanner 
        fields = ["image_file","title"]

class checkoutbannerForm(forms.ModelForm):
    class Meta:  
        model = checkoutbanner 
        fields = ["image_file","title"]        
        
        