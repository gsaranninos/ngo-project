# Generated by Django 2.2 on 2019-04-26 12:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ngo', '0017_auto_20190426_1232'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkout',
            name='order',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ngo.orders'),
        ),
    ]
