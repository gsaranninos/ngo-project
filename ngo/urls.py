from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from .import views
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [

#######################Frontend#####################
    path('', views.index,name='ngosites-index1'), 
    path('index', views.index,name='ngosites-index'), 
    path('about', views.about,name='ngosites-about'),
    path('checkout', views.checks,name='ngosites-checkout'),
    path('contact', views.contact,name='ngosites-contact'), 
    path('gallery', views.gallery,name='ngosites-gallery'), 
    path('products', views.products,name='ngosites-products'), 
    path('project', views.project,name='ngosites-project'),
    path('volunteers', views.volunteer,name='ngosites-volunteers'),
    path('blogs', views.blog,name='ngosites-blog'),

####################################################

    path('admin/',views.login,name='ngo-login'),
    path('register',views.register,name='ngo-register'),
    path('reset-password',views.resetpassword,name='registrations-password_reset_form'),
    path('reset_password_confirm/<tokens>/',views.resetpasswordconfirm,name='reset_password_confirm'),
    path('reset-password/complete/<tokens>', views.resetpasswordcomplete,name='registrations-password_reset_complete'),
    path('resetsuccess', views.resetpasswordsuccess),
    
    path('passwordchange', views.change_password,name='ngo-passwordchange'),
    path('dashboardpage', views.dash_board_pages,name='ngo-dashboardpage'),

    path('logout', views.logout, name='temp-logout'),

    path('enquirylist', views.enquirylist,name='ngo-enquiry'),
    path('enquirydelete/<int:id>', views.enquirydelete),

################Gallery#####################
    path('category', views.category,name='ngo-category'),
    path('deactivecategory/<int:id>', views.deactivecategorylist),
    path('activecategory/<int:id>', views.activecategorylist),
    path('categorylist', views.categorylist,name='ngo-categorylist'),
    path('categorydelete/<int:id>', views.categorydestroy,name='ngo-categorylist'),
    path('categoryedit/<int:id>/', views.categoryedit),  
    path('categoryupdate/<int:id>', views.categoryupdate),

    path('image', views.gallerycategories,name='ngo-categoryimage'),
    path('categoryimagelist', views.categoryimagelist,name='ngo-categoryimagelist'),
    path('categoryimageedit/<int:id>', views.categoryimageedit),
    path('categoryimageupdate/<int:id>', views.categoryimageupdate),
    path('categoryimagedelete/<int:id>', views.categoryimagedelete),
    path('ajax/galllery/filter', views.filterGallery,name='filter-gallery'),
#############################################################################

    path('projectadd', views.projectadd,name='ngo-project'),
    path('projectlist', views.projectlist,name='ngo-projectlist'),
    path('projectedit/<int:id>/', views.projectsedit),  
    path('projectupdate/<int:id>', views.projectsupdate),
    path('projectdelete/<int:id>', views.projectsdelete),



    path('volunteersadd', views.volunteersadd,name='ngo-volunteersadd'),
    path('volunteerslist', views.volunteerslist,name='ngo-volunteerslist'),
    path('volunteersedit/<int:id>/', views.volunteersedit),  
    path('volunteersupdate/<int:id>', views.volunteersupdate),
    path('volunteersdelete/<int:id>', views.volunteersdelete),


#############################Blogs#####################################
    path('blogcategories', views.blogcategories,name='ngo-blogcategories'),
    path('blogcategorieslist', views.bclist,name='ngo-blogcategorieslist'),
    path('blogcategoriesedit/<int:id>/', views.blogcategoryedit),  
    path('blogcategoryupdate/<int:id>', views.blogcategoryupdate),
    path('blogcategoriesdelete/<int:id>', views.blogcategorydestroy),
    path('categoriesdeactive/<int:id>', views.categorieslistdeactive),
    path('categoriesactive/<int:id>', views.categorieslistactive),

    path('blog', views.blogimage,name='ngo-blog'),
    path('bloglist', views.blogslist,name='ngo-bloglist'),
    path('blogsedit/<int:id>/', views.blogsedit),  
    path('blogsupdate/<int:id>', views.blogsupdate),
    path('blogsdelete/<int:id>', views.blogsdestroy),
    path('bloglistdeactive/<int:id>', views.blogdeactive),
    path('bloglistactive/<int:id>', views.blogactive),

    path('tags', views.tag,name='ngo-tags'),
    path('tagslist', views.taglist,name='ngo-tagslist'),
    path('tagsedit/<int:id>/', views.tagsedit),  
    path('tagsupdate/<int:id>', views.tagsupdate),
    path('tagsdelete/<int:id>', views.tagsdestroy),
    path('taglistdeactive/<int:id>', views.tagdeactive),
    path('taglistactive/<int:id>', views.tagactive),

    path('blogtags', views.blog_tag,name='ngo-blog_tags'),
    path('category/<slug:slug>', views.blogcategoryonclick,name='ngo-blogcategoryonclick'),
    path('blog/<slug:slug>', views.blogreadmoreonclick,name='ngo-blogreadmoreonclick'),
    path('blogtags/<slug:slug>', views.tagsonclick,name='ngo-blogreadmoreonclick'),

###############################################################################
    path('productadd',views.productadds,name='ngo-productadd'),
    path('productaddlist',views.productaddlist,name='ngo-productaddlist'),
    path('productaddedit/<int:id>/', views.productaddedit),
    path('productaddupdate/<int:id>', views.productaddupdate),  
    path('productadddelete/<int:id>', views.productadddelete),

    path('color',views.color,name='ngo-color'),
    path('colorlist',views.colorlist,name='ngo-colorlist'),
    path('coloredit/<int:id>/', views.coloredit),
    path('colorupdate/<int:id>', views.colorupdate),  
    path('colordelete/<int:id>', views.colordelete),

    path('addtocart/<int:id>', views.addtocarts,name='addtocartss'),
    path('choosingproductremove/<int:id>', views.choosingproductremove),
    path('productremove/<int:id>', views.productsremove),
    path('productdeliverydetails',views.deliverydetails,name='ngo-productdeliverydetails'),


    path('banner', views.banneradd,name='ngo-banner'),
    path('bannerlist', views.bannerlist,name='ngo-bannerlist'),
    path('banneredit/<int:id>/', views.banneredit),  
    path('bannerupdate/<int:id>', views.bannerupdate),
    path('bannerdelete/<int:id>', views.bannerdelete),
    #url('', include('newapp.urls')),
    
    path('hometestimonial', views.hometestimonialadd,name='ngo-hometestimonial'),
    path('hometestimoniallist', views.hometestimoniallist,name='ngo-hometestimoniallist'),
    path('hometestimonialedit/<int:id>/', views.hometestimonialedit),  
    path('hometestimonialupdate/<int:id>', views.hometestimonialupdate),
    path('hometestimonialdelete/<int:id>', views.hometestimonialdelete),
    
    path('aboutus', views.aboutusadd,name='ngo-aboutus'),
    path('aboutuslist', views.aboutuslist,name='ngo-aboutuslist'),
    path('aboutusedit/<int:id>/', views.aboutusedit),  
    path('aboutusupdate/<int:id>', views.aboutusupdate),
    path('aboutusdelete/<int:id>', views.aboutusdelete),
    
    
    path('aboutbanner', views.aboutbanneradd,name='ngo-aboutbanner'),
    path('aboutbannerlist', views.aboutbannerlist,name='ngo-aboutbannerlist'),
    path('aboutbanneredit/<int:id>/', views.aboutbanneredit),  
    path('aboutbannerupdate/<int:id>', views.aboutbannerupdate),
    path('aboutbannerdelete/<int:id>', views.aboutbannerdelete),


    path('projectbanner', views.projectbanneradd,name='ngo-projectbanner'),
    path('projectbannerlist', views.projectbannerlist,name='ngo-projectbannerlist'),
    path('projectbanneredit/<int:id>/', views.projectbanneredit),  
    path('projectbannerupdate/<int:id>', views.projectbannerupdate),
    path('projectbannerdelete/<int:id>', views.projectbannerdelete),

    path('volunteerbanner', views.volunteerbanneradd,name='ngo-volunteerbanner'),
    path('volunteerbannerlist', views.volunteerbannerlist,name='ngo-volunteerbannerlist'),
    path('volunteerbanneredit/<int:id>/', views.volunteerbanneredit),  
    path('volunteerbannerupdate/<int:id>', views.volunteerbannerupdate),
    path('volunteerbannerdelete/<int:id>', views.volunteerbannerdelete),


    path('gallerybanner', views.gallerybanneradd,name='ngo-gallerybanner'),
    path('gallerybannerlist', views.volunteerbannerlist,name='ngo-gallerybannerlist'),
    path('gallerybanneredit/<int:id>/', views.gallerybanneredit),  
    path('gallerybannerupdate/<int:id>', views.gallerybannerupdate),
    path('gallerybannerdelete/<int:id>', views.gallerybannerdelete),



    path('checkoutbanner', views.checkoutbanneradd,name='ngo-checkoutbanner'),
    path('checkoutbannerlist', views.checkoutbannerlist,name='ngo-checkoutbannerlist'),
    path('checkoutbanneredit/<int:id>/', views.checkoutbanneredit),  
    path('checkoutbannerupdate/<int:id>', views.checkoutbannerupdate),
    path('checkoutbannerdelete/<int:id>', views.checkoutbannerdelete),


    path('contactbanner', views.contactbanneradd,name='ngo-checkoutbanner'),
    path('contactbannerlist', views.contactbannerlist,name='ngo-contactbannerlist'),
    path('contactbanneredit/<int:id>/', views.contactbanneredit),  
    path('contactbannerupdate/<int:id>', views.contactbannerupdate),
    path('contactbannerdelete/<int:id>', views.contactbannerdelete),
    
    
]+static(settings.MEDIA_URL, document_root= settings.MEDIA_ROOT)


