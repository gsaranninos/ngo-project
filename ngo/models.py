from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


class UserProfileInfo(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=200,null=True)  
    last_name = models.CharField(max_length=100,null=True)

class usertokens(models.Model):  
        user = models.OneToOneField(User,on_delete=models.CASCADE,unique=False)
        tokens=models.CharField(max_length=200,unique=True)    


class enquiry(models.Model):  
        name = models.CharField(max_length=200)
        email= models.EmailField(max_length=200)
        phone = models.CharField(max_length=200)        
        subject = models.CharField(max_length=200)
        msg = models.CharField(max_length=200)
        created_at = models.DateTimeField(auto_now_add=True,null=True)
        updated_at = models.DateTimeField(auto_now=True,null=True)


class gallerycategory(models.Model):  
        category = models.ForeignKey('Usercategory',on_delete=models.CASCADE,null=True)
        image_file=models.ImageField(upload_to='gallery', blank=True,null=True)

class Usercategory(models.Model):  
        name = models.CharField(max_length=200)
        status=models.IntegerField(default=1,null=True) 
        deletestatus=models.IntegerField(default=1)         


class projects(models.Model):
        image_file=models.ImageField(upload_to='project', blank=True)
        contenttext=models.TextField()
        title=models.CharField(max_length=200,null=True)

class volunteers(models.Model):
        image_file=models.ImageField(upload_to='volunteers', blank=True)
        contenttext=models.TextField()
        created_at = models.DateTimeField(auto_now_add=True,null=True)
        updated_at = models.DateTimeField(auto_now=True,null=True)

class categories(models.Model):
        Name = models.CharField(max_length=200)
        slug= models.SlugField(max_length=200,null=True,blank=True)
        meta_title = models.CharField(max_length=200)
        meta_keyword = models.CharField(max_length=200)
        meta_description = models.TextField(max_length=200)
        status=models.IntegerField(default=1)
        created_at = models.DateTimeField(auto_now_add=True,null=True)
        updated_at = models.DateTimeField(auto_now=True,null=True)
        deletestatus=models.IntegerField(default=1)
    
        def _get_unique_slug(self):
            slug = slugify(self.Name)
            unique_slug = slug
            num = 1
            while categories.objects.filter(slug=unique_slug).exists():
                unique_slug = '{}-{}'.format(slug, num)
                num += 1
            return unique_slug
 
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = self._get_unique_slug()
            super().save(*args, **kwargs)
        
class tags(models.Model):
        Name = models.CharField(max_length=200)
        slug= models.SlugField(max_length=200,null=True,blank=True)
        meta_title = models.CharField(max_length=200)
        meta_keyword = models.CharField(max_length=200)
        meta_description = models.TextField(max_length=200)
        status=models.IntegerField(default=1)
        created_at = models.DateTimeField(auto_now_add=True)
        updated_at = models.DateTimeField(auto_now=True)
        deletestatus=models.IntegerField(default=1)

        def _get_unique_slug(self):
            slug = slugify(self.Name)
            unique_slug = slug
            num = 1
            while tags.objects.filter(slug=unique_slug).exists():
                unique_slug = '{}-{}'.format(slug, num)
                num += 1
            return unique_slug
 
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = self._get_unique_slug()
            super().save(*args, **kwargs)  

class blog_tags(models.Model):
        blog = models.ForeignKey('blogs',on_delete=models.CASCADE,null=True)
        tag = models.ForeignKey('tags',on_delete=models.CASCADE,null=True)
        created_at = models.DateTimeField(auto_now_add=True)
        updated_at = models.DateTimeField(auto_now=True)

class blogs(models.Model):
        category = models.ForeignKey('categories',on_delete=models.CASCADE,null=True)
        title = models.CharField(max_length=200)
        slug= models.SlugField(max_length=200,null=True,blank=True)
        description=models.TextField() 
        image_file=models.ImageField(upload_to='blog', blank=True)
        image_alt= models.CharField(max_length=200)
        image_title= models.CharField(max_length=200)
        meta_title= models.CharField(max_length=200)
        meta_keyword = models.CharField(max_length=200,null=True)
        meta_description = models.TextField(max_length=200)
        status=models.IntegerField(default=1)
        created_at = models.DateTimeField(auto_now_add=True,null=True)
        updated_at = models.DateTimeField(auto_now=True)        
        
        def _get_unique_slug(self):
            slug = slugify(self.title)
            unique_slug = slug
            num = 1
            while blogs.objects.filter(slug=unique_slug).exists():
                unique_slug = '{}-{}'.format(slug, num)
                num += 1
            return unique_slug
 
        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = self._get_unique_slug()
            super().save(*args, **kwargs)


class gettouchwithus(models.Model):  
        first_name = models.CharField(max_length=200)  
        last_name = models.CharField(max_length=100)
        mobile_number = models.CharField(max_length=100)
        email = models.EmailField(max_length=100)
        message = models.CharField(max_length=100)



class colors(models.Model):
    color = models.CharField(max_length=7)
    name = models.CharField(max_length=200)


class productadd(models.Model):
    #color = models.ForeignKey('colors',on_delete=models.CASCADE,null=True)
    name = models.CharField(max_length=200)
    price = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True,null=True)
    updated_at = models.DateTimeField(auto_now=True)     
    #image = models.ImageField(upload_to='product', blank=True)

class productcolorimage(models.Model):
    product = models.ForeignKey('productadd',on_delete=models.CASCADE,null=True)
    color = models.ForeignKey('colors',on_delete=models.CASCADE,null=True)
    image = models.ImageField(upload_to='product',blank=True,unique=False,null=True)   



class orders(models.Model):
    name=models.CharField(max_length=100)
    email= models.EmailField(max_length=100)    
    address=models.CharField(max_length=100,null=True)
    phone_no=models.CharField(max_length=100,null=True)
    transaction_id=models.CharField(max_length=100,null=True)
    transaction_status=models.CharField(max_length=100,null=True)
    total_amount=models.CharField(max_length=100,null=True)

class checkout(models.Model):
    order = models.ForeignKey('orders',on_delete=models.CASCADE,null=True)
    product=models.CharField(max_length=200)
    price = models.FloatField()  
    quantity = models.IntegerField(null=True)
    total = models.FloatField(max_length=200)
    
class banner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='project', blank=True,null=True)    
 
class hometestimonial(models.Model):
        image_file=models.ImageField(upload_to='project', blank=True)
        contenttext=models.TextField()  
        
class aboutus(models.Model):
        contenttext=models.TextField()
        title=models.CharField(max_length=200,null=True)        
        
class aboutbanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True)

class projectbanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True)

class gallerybanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True) 

class volunteerbanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True) 

class contactbanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True) 

class checkoutbanner(models.Model):  
    title = models.CharField(max_length=200,null=True)
    image_file=models.ImageField(upload_to='banner', blank=True,null=True)        
        
        
