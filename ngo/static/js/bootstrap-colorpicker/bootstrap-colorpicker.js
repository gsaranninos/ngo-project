$('#color-picker-size').colorpicker({
 
customClass: 'custom-size',
 
sliders: {
 
saturation: {
 
maxLeft: 250,
 
maxTop: 250
 
},
 
hue: {
 
maxTop: 250
 
},
 
alpha: {
 
maxTop: 250
 
}
 
}
 
});