from django.shortcuts import render,HttpResponse,HttpResponseRedirect,redirect
# Create your views here.
#from growelsapp.forms import UForm,newsandeventsForm,commentsForm,gettouchwithusForm,UserProfileInfoForm,enquiryForm,usertokensForm,enquiryForm,CategoriesForm,tagsForm,blog_tagsForm,blogsForm
#from growelsapp.models import UserProfileInfo,comments,newsandevents,usertokens,gettouchwithus,enquiry,categories,tags,blog_tags,blogs
from ngoapp.ngo.forms import checkoutbannerForm,contactbannerForm,gallerybannerForm,projectbannerForm,aboutbannerForm,volunteerbannerForm,aboutusForm,hometestimonialForm,bannerForm,usertokensForm,volunteersForm,ordersForm,checkoutForm,productcolorimageForm,productaddForm,enquiryForm,UserCategoryForm,galleryCategoryForm,UForm,UserProfileInfoForm,projectsForm,CategoriesForm,tagsForm,blog_tagsForm,blogsForm,gettouchwithusForm,colorsForm
from ngoapp.ngo.models import checkoutbanner,contactbanner,gallerybanner,projectbanner,volunteerbanner,aboutbanner,aboutus,hometestimonial,usertokens,banner,volunteers,orders,checkout,productcolorimage,productadd,enquiry,Usercategory,gallerycategory,projects,categories,tags,blog_tags,blogs,gettouchwithus,colors

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as dj_login,logout as dj_logout
from django.contrib import messages
from django.template.loader import render_to_string, get_template
from django.utils.html import strip_tags
from django.conf import settings
from django.core.mail import EmailMessage,EmailMultiAlternatives
from django.utils.crypto import get_random_string
from django.contrib.auth.forms import PasswordChangeForm
from django.template import loader 
from django.core.mail import send_mail
from django.conf.global_settings import DEFAULT_FROM_EMAIL
from django.contrib.auth.hashers import make_password
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
#import requests



def index(request):
    testimonial = hometestimonial.objects.raw("SELECT * FROM ngo_hometestimonial")
    ban = banner.objects.raw("SELECT * FROM ngo_banner")
    projectdata=projects.objects.all().reverse()[:1]
    categoryname=request.session.get('product_ids')
    about=aboutus.objects.all()
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0 
    #data = serializers.serialize('json', projectdata)
    #return HttpResponse(data, content_type="application/json")
    return render(request,'ngosites/index.html',{'projectdata':projectdata,'data':data,'ban':ban,'testimonial':testimonial,'about':about}) 

def about(request): 
    blogss=blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-id')[:3]
    categoryname=request.session.get('product_ids')
    about=aboutus.objects.all()
    aboutban =aboutbanner.objects.all()
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0 
    return render(request,'ngosites/about.html',{'data':data,'blogss':blogss,'about':about,'aboutban':aboutban}) 

def checks(request):
    categoryname=request.session.get('product_ids')
    proadds=productadd.objects.all()
    checkoutban =checkoutbanner.objects.all()
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=request.session.get('product_ids')
    if request.method == "POST":  
        forms = ordersForm(request.POST)
        
        unique_id = get_random_string(length=10)
        o=orders()
        o.name=request.POST['name']
        o.email=request.POST['email']
        o.phone_no=request.POST['phone_no']
        o.address=request.POST['address']
        o.total_amount=request.POST['total']
        o.transaction_id=unique_id
        o.transaction_status=unique_id
        o.save()
        order_id = o.id;  
        print(order_id)
        for ids in request.POST.getlist('ids'):
            price =request.POST['price'+ids]
            quantity = request.POST['quantity'+ids]
            total = int(request.POST['price'+ids]) * int(request.POST['quantity'+ids])
            check=checkout(order_id=order_id,product=ids,price=price,quantity=quantity,total=total)
            check.save()
    return render(request,'ngosites/checkout.html',{'data':data,'proadds':proadds,'checkoutban':checkoutban})     

def checkswedsr(request):
    #c=request.session['product_ids'].count()
    print(request.POST.getlist('ids'))
    categoryname=request.session.get('product_ids')
    proadds=productadd.objects.all()
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=request.session.get('product_ids')   
    if request.method == "POST": 
        forms = checkoutForm(request.POST)
        form=ordersForm(data=request.POST)
        print(form)
        if form.is_valid():
            unique_id = get_random_string(length=10)
            print(unique_id)
            c=orders()
            c.name=request.POST['name']
            c.email=request.POST['email']
            c.phone_no=request.POST['phone_no']
            c.address=request.POST['address']
            c.total_amount=request.POST['total']
            c.transaction_id=unique_id
            c.transaction_status=ins
            c.save()
        for ids in request.POST.getlist('ids'):
            price =request.POST['price'+ids]
            quantity = request.POST['quantity'+ids]
            total = request.POST['amount']
            check=checkout(product=ids,price=price,quantity=quantity,total=total)
            check.save()
    return render(request,'ngosites/checkout.html',{'data':data,'proadds':proadds}) 

def contact(request):
    contactban =contactbanner.objects.all()
    categoryname=request.session.get('product_ids')
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0  
    if request.method == "POST":  
        forms = enquiryForm(request.POST)
        if forms.is_valid():
            c=enquiry()
            c.name=request.POST['name']
            c.email=request.POST['email']
            c.phone=request.POST['phone']
            c.subject=request.POST['subject']
            c.msg=request.POST['msg']
            c.save()
            if c:
                email_template_data = {
                        'email': c.email,
                        'message' : c.msg,
                        'name' : c.name,
                        'subject' : c.subject,
                        'domain': request.META['HTTP_HOST'],
                        'protocol': 'http',
                    }  
            email_template_name = 'ngo/enquiryemail.html'
            subject = 'enquiryemail'
            email = loader.render_to_string(email_template_name, email_template_data)
            
            
            message=EmailMultiAlternatives(subject,email,settings.EMAIL_HOST_USER, [c.email],cc=["ecphasisinfotech2008@gmail.com","matthew@ninositsolution.com"]) 
            #html_template=get_template("temp/enquiryemail.html").render()
            message.attach_alternative(email,"text/html")
            message.send()
            
            
            subject = "Greetings"  
            msg     = render_to_string('ngo/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/ngo/templates/ngo/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to],cc=["ecphasisinfotech2008@gmail.com","matthew@ninositsolution.com"]) 
            html_template=get_template("ngo/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send() 
    return render(request,'ngosites/contact.html',{'data':data,'contactban':contactban}) 

def gallery(request):
    galleryban = gallerybanner.objects.all()
    categoryname=request.session.get('product_ids')
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0 
    categoryname=Usercategory.objects.filter(status=1).filter(deletestatus=1)
    accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    return render(request,'ngosites/gallery.html',{'categoryname':categoryname,'accountimage':accountimage,'data':data,'galleryban':galleryban}) 

def products(request):
    #categoryname=request.session.get('product_ids').count()  
    #print(request.session['product_ids'].count())
    proadds = productadd.objects.all().order_by('-updated_at') 

    pro=colors.objects.all()

    categoryname=request.session.get('product_ids')

    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=request.session.get('product_ids')
   
    #del request.session['product_ids']
    #proadds=prod.productcolorimage_set.all()
    #print(products.productcolorimage_set.all())

    
    
    #proadds = productcolorimage.objects.all()


    
    #categoryname=request.session['product_ids'].count()
    return render(request,'ngosites/products.html',{'proadds':proadds,'pro':pro,'data':data,'range':range(0,2)})

def project(request):
    projectban = projectbanner.objects.all()
    categoryname=request.session.get('product_ids')
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0 
    projectdata=projects.objects.all() 
    return render(request,'ngosites/project.html',{'projectdata':projectdata,'data':data,'projectban':projectban})

def volunteer(request):
    categoryname=request.session.get('product_ids')
    volunteerban = volunteerbanner.objects.all()
    if categoryname:
        data=productcolorimage.objects.filter(id__in=categoryname)
    else:
        data=0 
    projectdata=volunteers.objects.all() 
    return render(request,'ngosites/volunteers.html',{'projectdata':projectdata,'data':data,'volunteerban': volunteerban})


def blog(request):
    blogss=blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-id')
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    p = blogs.objects.order_by('title', 'description').last()
    e=blogs.objects.all().order_by('-id')[:3]
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('ngo/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/ngo/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("ngo/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send() 
    return render(request,'ngosites/blog.html',{'e':e,'p':p,'blogss':blogss,'tag':tag,'categorynames':categorynames})

def filterGallery(request):  
    #data = serializers.serialize('json', request)
    categoryname=colors.objects.all()
    if request.method == "GET":
        category_id = request.GET["category_id"]
        #return JsonResponse(category_id, safe=False)
        if category_id == "0":
            proadds=productadd.objects.all()
        else:    
            proadds=productadd.objects.filter(id=category_id)
        return render(request,"ngo/ajax-gallery.html",{'categoryname':categoryname,'gallery_datas':gallery_datas})

def login(request):
    if request.session.get('username',None) is not None:
        return redirect('/dashboardpage')    
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            request.session['username']=username
            print(username)
            if user.is_active:
                dj_login(request,user)
                return render(request, 'ngo/dashboardpage.html')
            else:
                return HttpResponse("Your account was inactive.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            messages.info(request, 'Enter valid username and password!')
            return render(request, 'ngo/login.html')
    else:
        return render(request, 'ngo/login.html')


def logout(request):
    dj_logout(request)
    del request.session
    return redirect("/admin")


def register(request):
    registered = False
    if request.method == 'POST':
        user_form = UForm(data=request.POST)
        profile_form = UserProfileInfoForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.first_name=request.POST['first_name']
            profile.last_name=request.POST['last_name']
            profile.save()
            registered = True
        else:
            print(user_form.errors,profile_form.errors)
            
    else:
        user_form = UForm()
        profile_form = UserProfileInfoForm()
    return render(request,'ngo/register.html',
                          {'user_form':user_form,
                           'profile_form':profile_form,
                           'registered':registered})

#LOGIN PAGE    
def resetpasswordconfirm(request,tokens):
        if request.method =='POST':
            new_password=request.POST['new_password2']
            #c=make_password(new_password)
            c=usertokens.objects.filter(tokens=tokens)
            for email in c:
                user_id = email.user_id
            if c:
                m=User.objects.get(id=user_id)    
                m.password=make_password(new_password)
                m.save()
                return render(request, "registrations/password_reset_complete.html",{'c':c}) 
            else:
                return HttpResponse("sorry invalid url") 
        return render(request, "registrations/password_reset_confirm.html")

#RESET PASSWORD 
def resetpassword(request):
    if request.method == "POST":  
        emails=request.POST['email']
        
        if User.objects.filter(email=emails).exists():
            c=User.objects.filter(email=emails)
            user = User.objects.get(email=emails)
            unique_id = get_random_string(length=20)
            if user is not None:
                        email_template_data = {
                    'email': emails,
                    'domain': request.META['HTTP_HOST'],
                    'tokens': unique_id,
                    'protocol': 'http',
                }
            if c:
                for user_id in c:
                    user_id=user_id.pk
                    if user_id:
                        print(user_id)
                        if usertokens.objects.filter(user_id=user_id).exists():
                            m=usertokens.objects.get(user_id=user_id)    
                            m.tokens=unique_id
                            m.save()    
                        else:
                            active=usertokens(user_id=user_id,tokens=unique_id)
                            active.save()       
                    email_template_name = 'registrations/password_reset_email.html'
                    subject = 'Password reset'
                    email = loader.render_to_string(email_template_name, email_template_data)
                    
                    message=EmailMultiAlternatives(subject,email,settings.EMAIL_HOST_USER, [emails]) 
            #html_template=get_template("temp/enquiryemail.html").render()
                    message.attach_alternative(email,"text/html")
                    message.send()
                    return render(request, 'registrations/password_reset_done.html')
        else:
            messages.info(request, 'you entered wrong email address!')
            return render(request, 'registrations/password_reset_form.html')            
    return render(request, 'registrations/password_reset_form.html')

def resetpasswordcomplete(request,tokens):
    delete=usertokens.objects.get(tokens=tokens)
    delete.delete()
    return redirect("/resetsuccess")

def resetpasswordsuccess(request):
    return render(request, 'registrations/password_reset_success.html')

@login_required 
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            #messages.success(request, 'Your password was successfully updated!')
            return redirect('/dashboardpage')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'ngo/passwordchange.html', {
        'form': form
    })    

def dash_board_pages(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    return render(request,'ngo/dashboardpage.html')    

def enquirylist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    enquiries = enquiry.objects.all().order_by('-updated_at')  
    return render(request,"ngo/enquiry.html",{'enquiries':enquiries})

#ENQUIRY DELETE 
def enquirydelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    Account = enquiry.objects.get(id=id)  
    Account.delete()  
    return redirect("/enquirylist")

def blogcategories(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = CategoriesForm(request.POST)
        if forms.is_valid():
            c=categories()
            c.Name=request.POST['Name']
           #c.slug=request.POST['slug']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword =request.POST['meta_keyword']
            c.meta_description=request.POST['meta_description']           
            c.save()
    else:
        forms = CategoriesForm()   
    return render(request,"ngo/blogcategories.html",{'forms': forms})


def bclist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = categories.objects.filter(deletestatus=1).order_by('-id')  
    return render(request,'ngo/blogcategorieslist.html', {'user':user}) 

#BLOG CATEGORIES STATUS DEACTIVE 
def categorieslistdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=categories.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/blogcategorieslist')

#BLOG CATEGORIES ACTIVE 
def categorieslistactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=categories.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/blogcategorieslist')

#category edit 
def blogcategoryedit(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = categories.objects.get(id=id)  
    return render(request,'ngo/blogcategoriesedit.html', {'employee':employee})  

#category upgate
def blogcategoryupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = categories.objects.get(id=id)
    if request.method == "POST":  
        form = CategoriesForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()  
            return redirect("/blogcategorieslist")
    else:
        form=  CategoriesForm()       
    return render(request, 'ngo/blogcategoriesedit.html', {'employee': employee,'form':form})

#BLOG CATEGORIES STATUS ACTIVE 
def blogcategorydestroy(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorydelete = categories.objects.get(id=id)
    categorydelete.deletestatus = 0  
    categorydelete.save() 
    return redirect("/blogcategorieslist")

#TAG
def tag(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = tagsForm(request.POST)
        if forms.is_valid():
            c=tags()
            c.Name=request.POST['Name']
            #c.slug=request.POST['slug']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword=request.POST['meta_keyword']
            c.meta_description=request.POST['meta_description']
            c.save() 
    else:
        forms = tagsForm()
    return render(request,"ngo/tags.html",{'forms': forms})

#TAG LIST
def taglist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = tags.objects.filter(deletestatus=1).order_by('-id')  
    return render(request,'ngo/tagslist.html', {'user':user})

#category edit 
def tagsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = tags.objects.get(id=id)  
    return render(request,'ngo/tagsedit.html', {'employee':employee})  

#tags upgate
def tagsupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = tags.objects.get(id=id)
    if request.method == "POST":  
        form = tagsForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()  
            return redirect("/tagslist") 
    else:
        form=  tagsForm()        
    return render(request, 'ngo/tagsedit.html', {'employee': employee,'form':form})

#tags delete
def tagsdestroy(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    tagsdelete = tags.objects.get(id=id) 
    tagsdelete.deletestatus = 0  
    tagsdelete.save()  
    return redirect("/tagslist") 



# TAG STATUS DEACTIVE 
def tagdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=tags.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/tagslist')

#TAG STAUS ACTIVE 
def tagactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=tags.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/tagslist')
#BLOG TAG
def blog_tag(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    blogss=blogs.objects.all()
    tagss=tags.objects.all()
    if request.method == "POST":  
        forms = blog_tagsForm(request.POST) 
        if forms.is_valid(): 
            s=blog_tags()
            blog=request.POST['blog']
            tag=request.POST['tag']
            obj=blog_tags(blog_id=blog,tag_id=tag)
            obj.save()
    return render(request,"ngo/blog_tags.html",{'tagss':tagss,'blogss':blogss})

#BLOG TAG LIST
def blogtaglist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = blog_tags.objects.all().order_by('-id')  
    return render(request,'ngo/list.html', {'user':user}) 

@csrf_exempt
@ensure_csrf_cookie
def blogimage(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tagss=tags.objects.filter(status=1).filter(deletestatus=1)
    if request.method == 'POST':  
        print(request.POST.getlist('tags'))
        forms = blogsForm(data=request.POST)
        blogtags = blog_tagsForm(data=request.POST) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "blog/"+unique_id+".png"
            c=blogs()
            category=request.POST['category']
            c.title=request.POST['title']
            #c.slug=request.POST['slug']
            c.description=request.POST['description']
            c.image_alt=request.POST['image_alt']
            c.image_title=request.POST['image_title']
            c.meta_title=request.POST['meta_title']
            c.meta_keyword=request.POST['meta_keyword']
            c.meta_description =request.POST['meta_description'] 
            image_file=image_name
            o=blogs(category_id=category,title=c.title,description=c.description,image_file=image_file,image_alt=c.image_alt,image_title=c.image_title,meta_title=c.meta_title,meta_keyword=c.meta_keyword,meta_description=c.meta_description)
            o.save()
            blog_id = o.id;  
            print(blog_id)
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((600,600), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT+image_name;
            imageresize.save(image_url,quality=100)
        for tag in request.POST.getlist('tags'): 
            blog_tag=blog_tags(blog_id=blog_id,tag_id=tag)
            blog_tag.save()   
    return render(request,"ngo/blog.html",{'categorynames':categorynames,'tagss':tagss}) 
        
#BLOG LIST
def blogslist(request): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    user = blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-id')
    return render(request,'ngo/bloglist.html', {'user':user}) 
   
#blog edit 
def blogsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = blogs.objects.get(id=id)
    categorynames=categories.objects.all() 
    tagss=tags.objects.all() 
    return render(request,'ngo/blogedit.html', {'tagss':tagss,'employee':employee,'categorynames':categorynames})  

#blog upgate
def blogsupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = blogs.objects.get(id=id)
    categorynames=categories.objects.all()
    if request.method == "POST":  
        form = blogsForm(request.POST,request.FILES,instance = employee)  
        if form.is_valid(): 
            if request.FILES:
                    image = request.FILES['image_file']
                    unique_id = get_random_string(length=10)
                    image_name = "blog/"+unique_id+".png"
                    s=blogs()
                    image_file=image_name
                    #active=blogs.objects.get(id=id)
                    employee.image_file=image_name
                    employee.save()
                    try:
                        from PIL import Image, ImageOps
                    except ImportError:
                        import Image
                        import ImageOps
                    image = Image.open(image)
                    imageresize = image.resize((600,600), Image.ANTIALIAS)
                    image_url = settings.MEDIA_ROOT +image_name;
                    imageresize.save(image_url,quality=100) 
            form.save()  
            return redirect("/bloglist")  
    return render(request, 'ngo/bloglist.html', {'employee': employee,'categorynames':categorynames})

# blog delete
def blogsdestroy(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    blogsdelete = blogs.objects.get(id=id)  
    blogsdelete.delete()  
    return redirect("/bloglist")


# BLOG STATUS DEACTIVE 
def blogdeactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    deactive=blogs.objects.get(id=id)
    deactive.status=0
    deactive.save()
    return redirect('/bloglist')

#BLOG STAUS ACTIVE 
def blogactive(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=blogs.objects.get(id=id)
    active.status=1
    active.save()
    return redirect('/bloglist')


def blogcategoryonclick(request,slug):

    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    p = blogs.objects.order_by('title', 'description').last()
    e=blogs.objects.all().order_by('-id')[:3]
    print(slug)   
    category_datas = categories.objects.filter(slug=slug)

    for category in category_datas:
        category_id = category.pk

    # category_id = category.pk
    #print(category_id)

    blog_datas = blogs.objects.filter(category_id=category_id).order_by('-updated_at')
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()    
    return render(request, 'ngo/blogcategoryonclick.html',{'e':e,'p':p,'blog_datas':blog_datas,'tag':tag,'categorynames':categorynames})



# BLOG READ MORE  
def blogreadmoreonclick(request,slug):
    
    p = blogs.objects.order_by('title', 'description').last()
    e=blogs.objects.all().order_by('-id')[:3]
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    blog=blogs.objects.all()
    print(slug)   
    blog_datas = blogs.objects.filter(slug=slug)

    for blog in blog_datas:
        blog_id = blog.pk

    #data = serializers.serialize('json', blog_datas)
    #return HttpResponse(data, content_type="application/json")    

    # category_id = category.pk
    #print(category_id)

    blog_datas = blogs.objects.filter(id=blog_id) 
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()
    return render(request, 'ngo/blogreadmoreonclick.html',{'p':p,'e':e,'categorynames':categorynames,'blog_datas':blog_datas,'tag':tag})



# TAGS ON CLICK FROM BLOG PAGE 
def tagsonclick(request,slug):
    p = blogs.objects.order_by('title', 'description').last()
    e=blogs.objects.all().order_by('-id')[:3]
    categorynames=categories.objects.filter(status=1).filter(deletestatus=1)
    tag=tags.objects.filter(status=1).filter(deletestatus=1)
    print(slug)
    #accountimage = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1))
    blog=blogs.objects.filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-updated_at') 
    print(slug)  
    tag_datas = tags.objects.filter(slug=slug)

    for tagss in tag_datas:
        tag_id= tagss.pk
    
    blog_datas = blog_tags.objects.filter(tag_id=tag_id)
    
    blog_ids = []
    for blog_data in blog_datas:
        blog_ids.append(blog_data.blog_id)
  

    print(blog_ids)    
    blog_list=blogs.objects.filter(id__in=blog_ids).filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1)).order_by('-updated_at')   
    if request.method == "POST":  
        forms = gettouchwithusForm(request.POST)
        if forms.is_valid():
            c=gettouchwithus()
            c.first_name=request.POST['first_name']
            c.last_name=request.POST['last_name']
            c.mobile_number=request.POST['mobile_number']
            c.email=request.POST['email']
            c.message=request.POST['message']
            c.save()
            subject = "Greetings"  
            msg     = render_to_string('growelsapp/email-activation.html')
            to      =  c.email
            plain_message = strip_tags(msg)
            with open(settings.BASE_DIR + "/growelsapp/templates/growelsapp/email-activation.txt") as f:
                send_message=f.read()
            message=EmailMultiAlternatives(subject,send_message,settings.EMAIL_HOST_USER, [to]) 
            html_template=get_template("growelsapp/email-activation.html").render()
            message.attach_alternative(html_template,"text/html")
            message.send()
    return render(request, 'ngo/blogtagsonclick.html',{'p':p,'e':e,'categorynames':categorynames,'blog_list':blog_list,'tag':tag})





def category(request):   
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = UserCategoryForm(request.POST)
        if forms.is_valid():
            category=Usercategory()
            category.name=request.POST['name']
            category.save()
    else:
        forms = UserCategoryForm()
    return render(request,'ngo/category.html',{'forms': forms})

 




#category delete     
def categorydestroy(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    categorydelete = Usercategory.objects.get(id=id)
    categorydelete.deletestatus = 0  
    categorydelete.save() 
    return redirect("/categorylist")



    '''
    if gallerycategory.objects.filter(category_id=id).exists():
        messages.info(request, 'This record is depanded to another record!') 
        return redirect("/categorylist")
    else:    
        categorydelete.delete()
        messages.info(request, 'Deleted successfully!')  
        return redirect("/categorylist") 

    '''

#category edit 
def categoryedit(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    employee = Usercategory.objects.get(id=id)  
    return render(request,'ngo/categoryedit.html', {'employee':employee})  



#category upgate
def categoryupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    employee = Usercategory.objects.get(id=id)
    if request.method == "POST":  
        form = UserCategoryForm(request.POST, instance = employee)  
        if form.is_valid():  
            form.save()
            messages.info(request, 'Updated successfully!')  
            return redirect("/categorylist") 
    else:
        form= UserCategoryForm()    
    return render(request, 'ngo/categoryedit.html', {'employee': employee,'form':form})

def categorylist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorylist = Usercategory.objects.filter(deletestatus=1).order_by('-id')
    return render(request,"ngo/categorylist.html",{'categorylist':categorylist})

 
def deactivecategorylist(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categorydeactive=Usercategory.objects.get(id=id)
    categorydeactive.status=0
    categorydeactive.save()
    #messages.info(request, 'Record deactive successfully!')
    return redirect('/categorylist')


#ACTIVE STATUS
def activecategorylist(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    active=Usercategory.objects.get(id=id)
    active.status=1
    active.save()
    #messages.info(request, 'Record active successfully!')
    return redirect('/categorylist')



def gallerycategories(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    categoryname=Usercategory.objects.filter(status=1)
    if request.method == "POST":  
        forms = galleryCategoryForm(request.POST,request.FILES) 
        if forms.is_valid(): 
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "gallery/"+unique_id+".jpg"
            s=gallerycategory()
            category=request.POST['category']
            image_file=image_name
            obj=gallerycategory(image_file=image,category_id=category)
            obj.save()
    else:
        forms= galleryCategoryForm()    
    return render(request,'ngo/categoryimage.html',{'categoryname':categoryname,'forms':forms}) 


def categoryimagelist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = gallerycategory.objects.filter(category__in=Usercategory.objects.filter(status=1).filter(deletestatus=1)).order_by('-id')
    return render(request,"ngo/categoryimagelist.html",{'user':user})    


#CATEGORY DELETE
def categoryimagedelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = gallerycategory.objects.get(id=id)  
    user.delete()  
    return redirect("/categoryimagelist")


#category edit 
def categoryimageedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    galleryedit = gallerycategory.objects.get(id=id)
    categoryname=Usercategory.objects.all()              
    return render(request,'ngo/categoryimageedit.html', {'galleryedit':galleryedit,'categoryname':categoryname})  


def categoryimageupdate(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    galleryedit = gallerycategory.objects.get(id=id)
    categoryname=Usercategory.objects.all() 
    if request.method == "POST":  
        forms = galleryCategoryForm(request.POST, request.FILES,instance = galleryedit)  
        if forms.is_valid():  
            forms.save()
            return redirect("/categoryimagelist") 
    else:
        forms= galleryCategoryForm()
    return render(request, 'ngo/categoryimageedit.html', {'categoryname':categoryname,'galleryedit': galleryedit,'forms':forms}) 


 
def projectadd(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = projectsForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=projects()
            s.title=request.POST['title']
            s.contenttext=request.POST['contenttext']
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((300,300), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/project.html")

#TESTINOMIAL LIST
def projectlist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = projects.objects.all().order_by('-id')  
    return render(request,"ngo/projectlist.html",{'user':user})

#TESTINOMIAL EDIT
def projectsedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    edit = projects.objects.get(id=id)  
    return render(request,'ngo/projectedit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def projectsupdate(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = projects.objects.get(id=id)  
    form = projectsForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=projects()
            image_file=image_name
            active=projects.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((150,150), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT +image_name;
            imageresize.save(image_url,quality=100)
            return redirect("/projectlist") 
        else: 
            form.save()   
            return redirect("/projectlist")  
    return render(request, 'ngo/projectlist.html', {'update': update})



#TESTINOMIAL DELETE    
def projectsdelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    Account = projects.objects.get(id=id)  
    Account.delete()  
    return redirect("/projectslist")



    # mstring = []
    # for key in request.POST.iterkeys():  # "for key in request.GET" works too.
    #     # Add filtering logic here.
    #     valuelist = request.POST.getlist(key)
    #     mstring.extend(['%s=%s' % (key, val) for val in valuelist])
    # print ('&'.join(mstring))

    # for key, value in request.POST.items():
    #      print('Key: %s' % (key) ) 
    #      print('Value %s' % (value) )

    # print(request.FILES.getlist('image_file3'))   
    
    


def color(request):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    if request.method == "POST":  
        forms = colorsForm(request.POST) 
        if forms.is_valid():
            s=colors()
            s.color=request.POST['color']
            s.name=request.POST['name']
            #print(hex_to_rgb(s.color))
            s.save()  
    return render(request,"ngo/color.html")


def colorlist(request): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user=colors.objects.all().order_by('-id')
    return render(request,"ngo/colorlist.html",{'user':user})


def coloredit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = colors.objects.get(id=id)  
    return render(request,'ngo/coloredit.html', {'edit':edit})

def colorupdate(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = colors.objects.get(id=id)  
    form = colorsForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        form.save()   
        return redirect("/colorlist")  
    return render(request, 'ngo/colorlist.html', {'edit': edit})

def colordelete(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    Account = colors.objects.get(id=id)  
    Account.delete()  
    return redirect("/colorlist")


def productadds(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = colors.objects.all()
    if request.method == 'POST':  
        print(request.POST.getlist('color'))
        forms = productaddForm(data=request.POST)
        blogtags = productcolorimageForm(request.POST,request.FILES) 
        if forms.is_valid():
            c=productadd()
            c.name=request.POST['name']
            #c.slug=request.POST['slug']
            c.price=request.POST['price']
            o=productadd(name=c.name,price=c.price)
            o.save()
            product_id = o.id;  
            print(product_id)           
        for color in request.POST.getlist('color'):
                image = request.FILES['image_file'+color]
                unique_id = get_random_string(length=10)
                image_name = "products/"+unique_id+".png" 
                image_file=image_name
                try:
                    from PIL import Image, ImageOps
                except ImportError:
                    import Image
                    import ImageOps
                image = Image.open(image)
                imageresize = image.resize((150,150), Image.ANTIALIAS)
                image_url = settings.MEDIA_ROOT +image_name;
                imageresize.save(image_url,quality=100)
                blog_tag=productcolorimage(product_id=product_id,color_id=color,image=image_file)
                blog_tag.save()
    else:
        forms=productaddForm()            
    return render(request,"ngo/productadd.html",{'user':user,'forms':forms})

def productaddlist(request): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user=productadd.objects.all()
    image=productcolorimage.objects.all()
    return render(request,"ngo/productaddlist.html",{'user':user,'image':image})

def productaddedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = productadd.objects.get(id=id)  
    user = colors.objects.all()
    return render(request,'ngo/productaddedit.html', {'edit':edit,'user':user})

def productaddupdate(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = productadd.objects.get(id=id) 
    #pro=productcolorimage.objects.get(product_id=id)
    cids=request.POST.getlist('color')
    colorids=productcolorimage.objects.filter(color_id__in=cids).filter(product_id=id)
    form = productaddForm(request.POST,instance = edit)
    forms = productcolorimageForm(request.POST,request.FILES,instance = edit)
    fo=colorsForm(request.POST)
    if form.is_valid():
        form.save()   
    for color in request.POST.getlist('color'):
        if request.FILES:
                if productcolorimage.objects.filter(color_id__in=color).filter(product_id=id):
                    image = request.FILES['image_file'+color]
                    unique_id = get_random_string(length=10)
                    image_name = "products/"+unique_id+".png" 
                    image_file=image_name 
                    active=productcolorimage.objects.filter(product_id__in=id).filter(color_id__in=color)
                    active.image=image_file
                    active.save()
                    try:
                        from PIL import Image, ImageOps
                    except ImportError:
                        import Image
                        import ImageOps
                    image = Image.open(image)
                    imageresize = image.resize((150,150), Image.ANTIALIAS)
                    image_url = settings.MEDIA_ROOT +image_name;
                    imageresize.save(image_url,quality=100)
                else:
                    image = request.FILES['image_file'+color]
                    unique_id = get_random_string(length=10)
                    image_name = "products/"+unique_id+".png" 
                    image_file=image_name       
                    try:
                        from PIL import Image, ImageOps
                    except ImportError:
                        import Image
                        import ImageOps
                    image = Image.open(image)
                    imageresize = image.resize((150,150), Image.ANTIALIAS)
                    image_url = settings.MEDIA_ROOT +image_name;
                    imageresize.save(image_url,quality=100)
                    blog_tag=productcolorimage(product_id=id,color_id=color,image=image_file)
                    blog_tag.save()
    #return redirect("/productaddlist")  
    return redirect("/productaddlist")                 
    #return render(request, 'ngo/productaddlist.html', {'edit': edit})

def productadddelete(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    Account = productadd.objects.get(id=id)  
    Account.delete()  
    return redirect("/productaddlist")
    
def addtocarts(request,id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    #del request.session['product_ids']
    if request.session.get('product_ids', 0):
        #test_list_set = set(request.session.get('product_ids'))
        print(request.session['product_ids'])
        product_ids = request.session['product_ids']
        if id in set(request.session.get('product_ids')):
            print ("Element Exists") 
            return redirect("/products") 
        else:   
            product_ids.append(id)
            print(product_ids)
            request.session['product_ids'] = product_ids
            return redirect("/products")    
    else:
        product_ids=[]
        product_ids.append(id)
        request.session['product_ids'] = product_ids
        print(product_ids) 
    return redirect("/products")


def productsremove(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if id in set(request.session.get('product_ids')):  
        Account = request.session.get('product_ids')
        print(request.session.get('product_ids'))
        a=Account.index(id)
        del Account[a]
        print(Account)
        request.session['product_ids'] = Account
        print(Account)
        request.session.modified =True
        return redirect("/checkout")
        #print(request.session.get('product_ids'))
    else:
        return redirect("/checkout")

def choosingproductremove(request, id):
    if id in set(request.session.get('product_ids')):  
        Account = request.session.get('product_ids')
        print(request.session.get('product_ids'))
        a=Account.index(id)
        del Account[a]
        print(Account)
        request.session['product_ids'] = Account
        print(Account)
        request.session.modified =True
        #print(request.session.get('product_ids'))
    return redirect("/products")    

    #blog_list=blogs.objects.filter(id__in=blog_ids).filter(category__in=categories.objects.filter(status=1).filter(deletestatus=1))       

def deliverydetails(request):
    if request.method == "POST":  
        forms = ordersForm(request.POST)
        if forms.is_valid():
            c=orders()
            c.email=request.POST['email']
            #c.name=request.POST['name']
            c.phone_no=request.POST['phone_no']
            c.address=request.POST['address']
            c.save()
    return render(request,'ngosites/productdeliverydetails.html') 



def volunteersadd(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    if request.method == "POST":  
        forms = volunteersForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "volunteers/"+unique_id+".png"
            s=volunteers()
            s.contenttext=request.POST['contenttext']
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((300,300), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT +image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/volunteersadd.html")

#TESTINOMIAL LIST
def volunteerslist(request):
    if request.session.get('username',None) == None:
        return redirect('/admin')
    user = volunteers.objects.all().order_by('-id')  
    return render(request,"ngo/volunteerslist.html",{'user':user})

#TESTINOMIAL EDIT
def volunteersedit(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin')  
    edit = volunteers.objects.get(id=id)  
    return render(request,'ngo/volunteersedit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def volunteersupdate(request, id): 
    if request.session.get('username',None) == None:
        return redirect('/admin')
    edit = volunteers.objects.get(id=id)  
    form = volunteersForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "volunteers/"+unique_id+".png"
            s=volunteers()
            image_file=image_name
            active=projects.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((150,150), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT +image_name;
            imageresize.save(image_url,quality=100)
            return redirect("/volunteerslist") 
        else: 
            form.save()   
            return redirect("/volunteerslist")  
    return render(request, 'ngo/voluteerslist.html', {'update': update})



#TESTINOMIAL DELETE    
def volunteersdelete(request, id):
    if request.session.get('username',None) == None:
        return redirect('/admin') 
    Account = volunteers.objects.get(id=id)  
    Account.delete()  
    return redirect("/volunteerslist")
    
def banneradd(request):
    if request.method == "POST":  
        forms = bannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=banner()
            s.title=request.POST['title']
            print(s.title)
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT  + image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/banner.html")


def bannerlist(request):
    user = banner.objects.all().order_by('-id')  
    return render(request,"ngo/bannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def banneredit(request, id):
      
    edit = banner.objects.get(id=id)  
    return render(request,'ngo/banneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def bannerupdate(request, id): 
    edit = banner.objects.get(id=id)  
    form = bannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=banner()
            image_file=image_name
            active=projects.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/bannerlist")  
    return render(request, 'ngo/bannerlist.html', {'update': update})


def bannerdelete(request, id): 
    Account = banner.objects.get(id=id)  
    Account.delete()  
    return redirect("/bannerlist")
    
def hometestimonialadd(request):
    if request.method == "POST":  
        forms = hometestimonialForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=hometestimonial()
            s.contenttext=request.POST['contenttext']
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((261,261), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT  + image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/hometestimonial.html")


def hometestimoniallist(request):
    user = hometestimonial.objects.all().order_by('-id')  
    return render(request,"ngo/hometestimoniallist.html",{'user':user})

#TESTINOMIAL EDIT
def hometestimonialedit(request, id):
    edit = hometestimonial.objects.get(id=id)  
    return render(request,'ngo/hometestimonialedit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def hometestimonialupdate(request, id): 
    edit = hometestimonial.objects.get(id=id)  
    form = hometestimonialForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "project/"+unique_id+".png"
            s=hometestimonial()
            image_file=image_name
            active=projects.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((261,261), Image.ANTIALIAS)
            image_url = settings.MEDIA_ROOT  +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/hometestimoniallist")  
    return render(request, 'ngo/hometestimoniallist.html', {'update': update})


def hometestimonialdelete(request, id): 
    Account = hometestimonial.objects.get(id=id)  
    Account.delete()  
    return redirect("/hometestimoniallist")    
    
    

def aboutusadd(request):
    if request.method == "POST":  
        forms = projectsForm(request.POST,request.FILES) 
        if forms.is_valid():
            s=aboutus()
            s.title=request.POST['title']
            s.contenttext=request.POST['contenttext']
            s.save() 
    return render(request,"ngo/aboutus.html")

#TESTINOMIAL LIST
def aboutuslist(request):
    user = aboutus.objects.all()  
    return render(request,"ngo/aboutuslist.html",{'user':user})

#TESTINOMIAL EDIT
def aboutusedit(request, id):
      
    edit = aboutus.objects.get(id=id)  
    return render(request,'ngo/aboutusedit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def aboutusupdate(request, id): 
    edit = aboutus.objects.get(id=id)  
    form = aboutusForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        form.save()   
    return redirect("/aboutuslist")  
    #return render(request, 'ngo/projectlist.html', {'update': update})



#TESTINOMIAL DELETE    
def aboutusdelete(request, id): 
    Account = aboutus.objects.get(id=id)  
    Account.delete()  
    return redirect("/aboutuslist")    


def aboutbanneradd(request):
    if request.method == "POST":  
        forms = aboutbannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=aboutbanner()
            s.title=request.POST['title']
            print(s.title)
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/aboutbanner.html")


def aboutbannerlist(request):
    user = aboutbanner.objects.all()  
    return render(request,"ngo/aboutbannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def aboutbanneredit(request, id):
      
    edit = aboutbanner.objects.get(id=id)  
    return render(request,'ngo/aboutbanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def aboutbannerupdate(request, id): 
    edit = aboutbanner.objects.get(id=id)  
    form = aboutbannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=aboutbanner()
            image_file=image_name
            active=aboutbanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/aboutbannerlist")  
    return render(request, 'ngo/aboutbannerlist.html', {'update': update})


def aboutbannerdelete(request, id): 
    Account = aboutbanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/aboutbannerlist")


def projectbanneradd(request):
    if request.method == "POST":  
        forms = projectbannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=projectbanner()
            s.title=request.POST['title']
            print(s.title)
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/projectbanner.html")


def projectbannerlist(request):
    user = projectbanner.objects.all()  
    return render(request,"ngo/projectbannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def projectbanneredit(request, id):
      
    edit = projectbanner.objects.get(id=id)  
    return render(request,'ngo/projectbanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def projectbannerupdate(request, id): 
    edit = projectbanner.objects.get(id=id)  
    form = projectbannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=banner()
            image_file=image_name
            active=projectbanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/projectbannerlist")  
    return render(request, 'ngo/projectbannerlist.html', {'update': update})


def projectbannerdelete(request, id): 
    Account = projectbanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/projectbannerlist")




def gallerybanneradd(request):
    if request.method == "POST":  
        forms = gallerybannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=gallerybanner()
            s.title=request.POST['title']
            
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/gallerybanner.html")


def gallerybannerlist(request):
    user = gallerybanner.objects.all()  
    return render(request,"ngo/gallerybannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def gallerybanneredit(request, id):  
    edit = gallerybanner.objects.get(id=id)  
    return render(request,'ngo/gallerybanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def gallerybannerupdate(request, id): 
    edit = gallerybanner.objects.get(id=id)  
    form = gallerybannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=gallerybanner()
            image_file=image_name
            active=gallerybanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/gallerybannerlist")  
    return render(request, 'ngo/gallerybannerlist.html', {'update': update})


def gallerybannerdelete(request, id): 
    Account = gallerybanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/gallerybannerlist")


def volunteerbanneradd(request):
    if request.method == "POST":  
        forms = volunteerbannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=volunteerbanner()
            s.title=request.POST['title']
            
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/volunteerbanner.html")


def volunteerbannerlist(request):
    user = volunteerbanner.objects.all()  
    return render(request,"ngo/volunteerbannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def volunteerbanneredit(request, id):  
    edit = volunteerbanner.objects.get(id=id)  
    return render(request,'ngo/volunteerbanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def volunteerbannerupdate(request, id): 
    edit = volunteerbanner.objects.get(id=id)  
    form = volunteerbannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=volunteerbanner()
            image_file=image_name
            active=valunteerbanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/volunteerbannerlist")  
    return render(request, 'ngo/volunteerbannerlist.html', {'update': update})


def volunteerbannerdelete(request, id): 
    Account = valunteerbanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/volunteerbannerlist")



def contactbanneradd(request):
    if request.method == "POST":  
        forms = contactbannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=contactbanner()
            s.title=request.POST['title']
            
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/contactbanner.html")


def contactbannerlist(request):
    user = contactbanner.objects.all()  
    return render(request,"ngo/contactbannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def contactbanneredit(request, id):  
    edit = contactbanner.objects.get(id=id)  
    return render(request,'ngo/contactbanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def contactbannerupdate(request, id): 
    edit = contactbanner.objects.get(id=id)  
    form = contactbannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=contactbanner()
            image_file=image_name
            active=contactbanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/contactbannerlist")  
    return render(request, 'ngo/contactbannerlist.html', {'update': update})


def contactbannerdelete(request, id): 
    Account = contactbanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/contactbannerlist")


def checkoutbanneradd(request):
    if request.method == "POST":  
        forms = checkoutbannerForm(request.POST,request.FILES) 
        if forms.is_valid():
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=checkoutbanner()
            s.title=request.POST['title']
            s.image_file=image_name
            s.save() 
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/'+image_name;
            imageresize.save(image_url,quality=100) 
    return render(request,"ngo/checkoutbanner.html")


def checkoutbannerlist(request):
    user = checkoutbanner.objects.all()  
    return render(request,"ngo/checkoutbannerlist.html",{'user':user})

#TESTINOMIAL EDIT
def checkoutbanneredit(request, id):  
    edit = checkoutbanner.objects.get(id=id)  
    return render(request,'ngo/checkoutbanneredit.html', {'edit':edit}) 

#TESTINOMIAL UPDATE
def checkoutbannerupdate(request, id): 
    edit = checkoutbanner.objects.get(id=id)  
    form = contactbannerForm(request.POST,request.FILES, instance = edit)  
    if form.is_valid():
        if request.FILES:
            image = request.FILES['image_file']
            unique_id = get_random_string(length=10)
            image_name = "banner/"+unique_id+".png"
            s=contactbanner()
            image_file=image_name
            active=contactbanner.objects.get(id=id)
            active.image_file=image_file
            active.save()
            try:
                from PIL import Image, ImageOps
            except ImportError:
                import Image
                import ImageOps
            image = Image.open(image)
            imageresize = image.resize((1357,507), Image.ANTIALIAS)
            image_url = 'ngo/media/' +image_name;
            imageresize.save(image_url,quality=100) 
        form.save()   
        return redirect("/checkoutbannerlist")  
    return render(request, 'ngo/checkoutbannerlist.html', {'update': update})


def checkoutbannerdelete(request, id): 
    Account = checkoutbanner.objects.get(id=id)  
    Account.delete()  
    return redirect("/checkoutbannerlist")        
    